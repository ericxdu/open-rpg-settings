OpenD6 Elementals

An alternate world that is inhabited by normal humans and the magical 
human creatures called Elementals who roam among them. Each "race" of 
Elemental should be characterized by a natural element, have super-human 
lifespan and physical/mental attributes, some uncanny or mystical 
psionic or magic abilities, and a major "character flaw" tied to those 
abilities.

Two major Elementals could be: the *Stonemen* (characterized by cold, 
calculation, and earth) have hyper-active cognition and eternal, 
nigh-invulnerable bodies, but an overpowering hunger for fresh-killed 
human flesh; and the *Shapeshifters* (characterized by emotion, 
unpredictability, and fire) who can transform into giant beasts but are 
controlled by a seething rage.

Among normal humans are Latents, who are born with a subtle power and 
the potential to become an Elemental.

Use OpenD6 Adventure.

Notes on the races:

Since the Shapeshifters' beast forms will be rather formidable, they should
have one relatively minor ability, like telepathic communication with other
Shapeshifters.

Since the Stonemen are rather formidable, they should each have an individual,
unique psionic ability.

Aesthicially there is room for two more elementals; perhaps one 
characterized by water and another by air like the Djinni and the 
Mermen. Try to use the Shapeshifters and the Stonemen as examples. Perhaps one
race could be characterized by wisdom. Look also at mytholigical humanoid
creatures in folklore.
